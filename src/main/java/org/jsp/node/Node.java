package org.jsp.node;

public abstract class Node implements java.io.Serializable{
	protected String value=null;
	private Node left;
	private Node right;
	
	
	public Node() {
		this.left = null;
		this.right = null;
	}
	
	public Node(Node left) {
		this.left = left;
		this.right = null;
	}
	
	public Node(Node left, Node right) {
		this.left = left;
		this.right = right;
	}

	abstract public String getValue();
	
	abstract public void setValue(String value);

	public Node getLeft() {
		return left;
	}

	public void setLeft(Node left) {
		this.left = left;
	}

	public Node getRight() {
		return right;
	}

	public void setRight(Node right) {
		this.right = right;
	}
	
	public boolean isDigit() {
		return this.getClass().equals(Digit.class);
	}
	
	
	abstract public boolean removeElement();
}
