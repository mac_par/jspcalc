package org.jsp.node;

public class Operand extends Node implements java.io.Serializable{
	
	public Operand(String operand) {
		super();
		setValue(operand);
	}
	
	public Operand(String operand, Node left) {
		super(left);
		setValue(operand);
	}
	
	public Operand(String operand, Node left, Node right) {
		super(left,right);
		setValue(operand);
	}
	
	@Override
	public String getValue() {
		return this.value;
	}

	@Override
	public void setValue(String value) {
		this.value=value;
		
	}

	@Override
	public boolean removeElement() {
		return true;
	}

}
