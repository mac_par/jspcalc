package org.jsp.node;
import java.util.regex.Pattern;

public class Digit extends Node implements java.io.Serializable{
	private final static Pattern isDIGIT=Pattern.compile("\\d{1,}");
	
	private Integer scale;
	private boolean period;
	private Integer fraction;

	public Digit(String scale) {
		super();
		this.period = false;
		this.fraction = null;
		if(scale.length()>1) {
			for(int i=0;i<scale.length();i++) {
				this.setValue(new Character(scale.charAt(i)).toString());
			}
		}else {
			this.scale = new Integer(scale);
		}
	}

	public Digit(Integer scale, Node left) {
		super(left);
		this.scale = scale;
		this.period = false;
		this.fraction = null;
	}

	public Digit(Integer scale, Node left, Node right) {
		super(left, right);
		this.scale = scale;
		this.period = false;
		this.fraction = null;
	}

	@Override
	public String getValue() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.scale);
		if (this.period) {
			builder.append('.');
			if (this.fraction != null) {
				builder.append(this.fraction);
			}
		}
		
		return builder.toString();
	}

	
	@Override
	public void setValue(String value) {
		if(isDIGIT.matcher(value).matches()) {
			if(this.period) {
				setFraction(new Integer(value));
			}else {
				setScale(new Integer(value));
			}
		}else {
			if(value.equals(".")) {
			setPeriod(true);
			}else if(value.equals("+/-") && this.scale!=null) {
				this.scale=-this.scale;
			}
		}

	}

	
	public void setScale(Integer scale) {
		if(this.scale==null) {
			this.scale=scale;
		}else {
			this.scale*=10;
			this.scale+=scale;
		}
	}

	public void setPeriod(boolean period) {
		this.period = period;
	}

	public void setFraction(Integer fraction) {
		if(this.fraction==null) {
			this.fraction=fraction;
		}else {
			this.fraction*=10;
			this.fraction+=fraction;
		}
	}

	@Override
	public boolean removeElement() {
		
		String l=null;
		if(this.fraction!=null && this.period) {
			l=this.fraction.toString();
			if(l.length()>1) {
				this.fraction/=10;
			}else {
				this.fraction=null;
			}
			return false;
		}
		
		if(this.period) {
			this.period=false;
			return false;
		}
		
		if(this.scale!=null) {
			l=this.scale.toString();
			if(l.length()>1) {
				this.scale/=10;
				return false;
			}else {
				this.scale=null;
				return true;
			}
		}
		
		return true;
	}
	
}
