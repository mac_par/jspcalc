package org.jsp.bean;
import static java.lang.Math.sqrt;

public class CalcOperations implements java.io.Serializable{
	
	public static String calculate(String d1, String operand,String d2) throws Exception{
		try {
			Double left=new Double(d1);
			Double right=null;
			if(d2!=null){
				right=new Double(d2);
			}
			
			String result="";
			
			switch(operand){
			case "+":
				result+=(left+right);
				break;
			case "-":
				result+=(left-right);
				break;
			case "*":
				result+=(left*right);
				break;
			case "/":
				result+=(left/right);
				break;
			case "sqrt":
				result+=(sqrt(left));
				break;
			case "%":
				result+=(left%right);
				break;
			default:
				throw new Exception(operand +" is not supported");
			}
			
			return result;
		}catch(Exception ex) {
			throw ex;
		}
	}
	
}
