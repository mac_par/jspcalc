package org.jsp.bean.picker;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.awt.Color;

@Named("picker")
@ApplicationScoped
public class ColorPicker {
	private String screen;
	private String background;
	private String font;
	private boolean bRender;

	public ColorPicker() {
		reset();
	}

	public String getScreen() {
		return this.screen;
	}

	public void setScreen(String screen) {
		this.screen=screen;
	}

	public String getBackground() {
		return this.background;
	}

	public void setBackground(String background) {
		this.background = background;
	}

	public boolean isbRender() {
		return bRender;
	}

	public void render() {
		this.bRender=!this.bRender;
	}
	

	public String getFont() {
		return this.font;
	}

	public void setFont(String font) {
		this.font = font;
		
	}

	public String buttonLabel() {
		return (this.bRender)?"Hide":"Settings";
	}
	
	public void reset() {
		screen=background="ff";
		this.font="0";
		this.bRender=false;
	}
}
