package org.jsp.bean;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ManagedBean
public class CalcController implements java.io.Serializable{
	@Inject
	private CalcBean2 bean;
	
	public void clear() {
		bean.clear();
	}
	
	public void expression(String exp) {
		bean.setExpression(exp);
	}
	
	public String result() {
		return bean.getResult();
	}
	
	public String disabled() {
		return bean.getWrong();
	}
	public void removeElement() {
		bean.removeElement();
	}
}
