package org.jsp.bean;
import org.jsp.node.Node;
import org.jsp.node.Digit;
import org.jsp.node.Operand;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CalcBean {
	private static final String ERROR_MSG = "ERROR";
	private Node head = null;
	private boolean correct;
	

	public CalcBean() {
		this.correct=true;
	}
	
	public void setExpression(String exp) {
		// if clear, then clear it
		if (exp.equals("C")) {
			this.correct=true;
			clear();
		}

		boolean bDigit = isDigit(exp);
		if (this.head == null) {
			if (bDigit) {
				this.head = new Digit(exp);
			} else {
				return;
			}
		} else {
			if (this.head.getValue().equals("="))
				return;

			boolean bPeriod = exp.equals(".");
			if (!bDigit && !bPeriod) {
				if(exp.equals("+/-")) {
					if(this.head.isDigit()) {
						this.head.setValue(exp);
					}else if(this.head.getRight()!=null){
						this.head.getRight().setValue(exp);
					}
				}else {
					Node n = new Operand(exp, this.head);
					this.head = n;
				}
			} else {
				/*if (this.head.getValue().equals("+/-")) {
					return;
				} else */if (this.head.isDigit()) {
					this.head.setValue(exp);
					return;
				} else {
					if (this.head.getRight() != null) {
						this.head.getRight().setValue(exp);
						return;
					} else {
						this.head.setRight(new Digit(exp));
					}
				}
			}
		}
	}

	public void removeElement() {
		if (this.head != null) {
			if (this.head.getRight() != null) {
				if(this.head.getRight().removeElement()) {					
					this.head.setRight(null);
				}

			} else if (this.head.getLeft() != null) {
				if (this.head.getLeft().removeElement()) {
					Node tmp = this.head;
					this.head = this.head.getLeft();
					tmp.setValue(null);
					tmp = null;
				}
			} else {
				if(this.head.removeElement()) {
					this.head.setValue(null);
					this.head = null;
				}
				
			}
		}
	}

	public String getResult() {
		if (this.head != null) {
			if (this.head.isDigit()) {
				return this.head.getValue();
			} else {
				if (this.head.getValue().equals("=")) {
					try {
						String result= calculate(this.head);
						this.clear();
						this.head=new Digit(result);
						this.correct=true;
						return result;
					} catch (Exception ex) {
						clear();
						this.correct=false;
						return ERROR_MSG;
					} 

				} else {
					return currentEquation(this.head);
				}
			}
		} else {
			return "";
		}
	}

	private String currentEquation(Node node) {
		if (node == null)
			return "";

		String eq = this.currentEquation(node.getLeft());

		eq+=node.getValue();
		if(node.getRight()!=null) {
			eq+=node.getRight().getValue();
		}
		
		return eq;
	}

	private String calculate(Node node) throws Exception {
		if (node == null)
			return "";

		String res = calculate(node.getLeft());
		if (node.isDigit()) {
			return node.getValue();
		} else {
			if (node.getValue().equals("="))
				return res;
			else {
				String oper = node.getValue();
				String snd = (node.getRight() != null) ? node.getRight().getValue() : null;
				return CalcOperations.calculate(res, oper, snd);
			}
		}

	}

	public void clear() {
		clearNode(this.head);
		this.head = null;
	}

	private void clearNode(Node node) {
		if (node == null)
			return;

		clearNode(node.getLeft());
		//node.setValue(null);
		node.setLeft(null);
		node.setRight(null);
	}

	private boolean isDigit(String str) {
		Matcher matcher = Pattern.compile("\\d").matcher(str);
		return matcher.matches();
	}

	public String getWrong() {
		return (this.correct)?"":"disabled";
	}
}
