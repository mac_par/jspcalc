<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="calc" class="org.jsp.bean.CalcBean" scope="application" />
<jsp:setProperty property="expression" name="calc" param="expression" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="../css/style.css" rel="stylesheet" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>JSP Kalculator</title>
</head>
<body>
	<div id="calc_cont">
	<p>JSP Kalkulator</p>
	<br />
		<form id="kalkulator" method="post">
			<table>
				<tr>
					<td colspan="4" id="result">${calc.result}</td>
					<td><input type="submit" name="expression" value="C" /></td>
				</tr>
				<tr>
					<td><input type="submit" name="expression" value="9"
						${calc.wrong} /></td>
					<td><input type="submit" name="expression" value="8"
						${calc.wrong} /></td>
					<td><input type="submit" name="expression" value="7"
						${calc.wrong} /></td>
					<td><input type="submit" name="expression" value="/"
						${calc.wrong} /></td>
					<td><input type="submit" name="expression" value="sqrt"
						${calc.wrong} /></td>
				</tr>
				<tr>
					<td><input type="submit" name="expression" value="4"
						${calc.wrong} /></td>
					<td><input type="submit" name="expression" value="5"
						${calc.wrong} /></td>
					<td><input type="submit" name="expression" value="6"
						${calc.wrong} /></td>
					<td><input type="submit" name="expression" value="*"
						${calc.wrong} /></td>
					<td><input type="submit" name="expression" value="%"
						${calc.wrong} /></td>
				</tr>
				<tr>
					<td><input type="submit" name="expression" value="1"
						${calc.wrong} /></td>
					<td><input type="submit" name="expression" value="2"
						${calc.wrong} /></td>
					<td><input type="submit" name="expression" value="3"
						${calc.wrong} /></td>
					<td><input type="submit" name="expression" value="-"
						${calc.wrong} /></td>
					<td rowspan="2"><input type="submit" name="expression"
						value="=" id="equals" ${calc.wrong} /></td>
				</tr>
				<tr>
					<td><input type="submit" name="expression" value="0"
						${calc.wrong} /></td>
					<td><input type="submit" name="expression" value="."
						${calc.wrong} /></td>
					<td><input type="submit" name="expression" value="+/-"
						${calc.wrong} /></td>
					<td><input type="submit" name="expression" value="+"
						${calc.wrong} /></td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>